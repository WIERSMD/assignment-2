﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour
{

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform smokeHolder;
    public Transform fireHolder;

    public enum MapDataFormat
    {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject smokePrefab;
    public GameObject firePrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> smokePositions;
    public List<Vector3> firePositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole()
    {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent)
    {
        for ( int i = parent.childCount - 1; i >= 0; i-- )
        {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel()
    {
        //clears instantiated game objects from the map
        //to make way for new data to be loaded
        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(smokeHolder);
        DestroyChildren(fireHolder);

        // Finds the folder path for the maps path and gets the appropriate filename
        //
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // Parses the .tmx file and stops at specified strings
        // and converts certain attributes within the text into variables to 
        // get the imported map data
        {
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            using ( XmlReader reader = XmlReader.Create(new StringReader(content)) )
            {

                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;


                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                //changes the data of the map depending on the type of format it's saved as
                switch ( encodingType )
                {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                mapDataString = reader.ReadElementContentAsString().Trim();


                //clears the position values on the xml file in case of changes to source
                turretPositions.Clear();
                smokePositions.Clear();
                firePositions.Clear();

                //loops through each object in the object group header and assigns it a position,
                // repeating for each different object type of each name
                if ( reader.ReadToFollowing("objectgroup") )
                {

                    if ( reader.GetAttribute("name") == "Smoke Particles" )
                    {
                        if ( reader.ReadToDescendant("object") )
                        {
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                smokePositions.Add(new Vector3(x, -y, 0));

                            } while ( reader.ReadToNextSibling("object") );
                        }

                    }
                    reader.ReadToNextSibling("objectgroup");
                    if ( reader.GetAttribute("name") == "Fire Particles" )
                    {


                        if ( reader.ReadToDescendant("object") )
                        {
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                firePositions.Add(new Vector3(x, -y, 0));

                            } while ( reader.ReadToNextSibling("object") );
                        }
                    }
                    if ( reader.GetAttribute("name") == "Turret" )
                    {
                        if ( reader.ReadToDescendant("object") )
                        {
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                turretPositions.Add(new Vector3(x, -y, 0));

                            } while ( reader.ReadToNextSibling("object") );
                        }
                    }
                }







            }

            //compiles list of tile IDs based on format of file being read, reading the map data differently for each type
            switch ( mapDataFormat )
            {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while ( index < bytes.Length )
                    {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach ( string line in lines )
                    {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach ( string value in values )
                        {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {

            //adjusts the tile width to match the resolution of the game
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            //adjusts centre points of the map tiles so they can be placed properly 
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // applies formatting to imported sprite sheet which would normally be imported by unity if not generated in-editor
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // splits the sprites for the maps into a grid and creates sprites from them
        {
            mapSprites.Clear();

            for ( int y = spriteSheetRows - 1; y >= 0; y-- )
            {
                for ( int x = 0; x < spriteSheetColumns; x++ )
                {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // creates instances of tile prefabs and places them in a grid from left to right, top to bottom
        //clearing the existing tiles first to prevent overlap
        //
        //Then adds tiles to list in order to clear them later in the event another map is loaded
        {
            tiles.Clear();

            for ( int y = 0; y < mapRows; y++ )
            {
                for ( int x = 0; x < mapColumns; x++ )
                {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // instantiates and places turret objects around the map at turret locations within the turret holder objects
        //also searches for each particle object to places particles in the same manner. Could also be applied to change loaded properties of particles
        {
            foreach ( Vector3 turretPosition in turretPositions )
            {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }
        {
            foreach ( Vector3 smokePosition in smokePositions )
            {
                GameObject smoke = Instantiate(smokePrefab, smokePosition + mapCenterOffset, Quaternion.identity) as GameObject;
                smoke.name = "Smoke";
                smoke.transform.parent = smokeHolder;
            }
        }
        {
            foreach ( Vector3 firePosition in firePositions )
            {
                GameObject fire = Instantiate(firePrefab, firePosition + mapCenterOffset, Quaternion.identity) as GameObject;
                fire.name = "Fire";
                fire.transform.parent = fireHolder;
            }
        }



        //displays time when the level was loaded
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


