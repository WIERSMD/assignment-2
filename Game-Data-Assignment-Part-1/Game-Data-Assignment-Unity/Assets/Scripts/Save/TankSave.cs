﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    public Tank tank;
    private string jsonString;

    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    //get script reference and create new instance of data
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //serialize the information of the object
    //and sends it back to the save manager as a string to put in the JSON file
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //reverses the serialization and applies the game data to the object
    //in order to return it to its previous state
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}