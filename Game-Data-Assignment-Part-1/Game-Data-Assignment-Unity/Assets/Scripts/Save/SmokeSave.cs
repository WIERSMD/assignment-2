﻿using UnityEngine;
using System;


[Serializable]

public class SmokeSave : Save
{
    public Smoke smoke;
    public Data data;
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public float startSize;
        public float startSpeed;
        public float color;
    }

    //get script reference and create new instance of data
    void Awake()
    {
        smoke = GetComponent<Smoke>();
        data = new Data();
    }
    
    // Serializes the smoke save data and sends it off to the GameSaveManager when called
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = smoke.transform.position;
        data.startSize = smoke.startSize;
        data.startSpeed = smoke.startSpeed;
        data.color = smoke.smokeColorValue;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }


    // "Unpacks" the JSON data from the saved file
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        smoke.transform.position = data.position;
        smoke.startSize = data.startSize;
        smoke.startSpeed = data.startSpeed;
        smoke.smokeColorValue = data.color;
        smoke.name = "Smoke";
    }

}
