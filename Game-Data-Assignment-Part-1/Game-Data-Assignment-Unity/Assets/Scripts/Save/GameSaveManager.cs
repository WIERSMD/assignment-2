﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour
{

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //assigns a game data path location to the specified string
        //and initiates a list of objects to be saved
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }

    //function used to add an item to the list of items to save
    public void AddObject(string item)
    {
        saveItems.Add(item);
    }

    //function to save object data
    public void Save()
    {

        //clears previous list of items from last save
        saveItems.Clear();

        //finds every object the inherits from the Save class
        //putting them in a list in order
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            print(saveableObject);
            saveItems.Add(saveableObject.Serialize());
        }

        //writes each of the serialized items to the the save file
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath))
        {
            foreach (string item in saveItems)
            {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //loads the saved data by reloading the scene
    public void Load()
    {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    //reinstantiates the saved items and variables when the new scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }


    //reads through the save data line-by-line and adds it to the saveItems list
    //to prepare it to be reloaded into the scene.
    void LoadSaveGameData()
    {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath))
        {
            while (gameDataFileStream.Peek() >= 0)
            {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0)
                {
                    saveItems.Add(line);
                }
            }
        }
    }

    //moves through list of saved objects and clears them to prevent too many old assets
    void DestroyAllSaveableObjectsInScene()
    {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            Destroy(saveableObject.gameObject);
        }
    }

    //creates information on each object to be saved in the json file
    //so it can be reinstantiated with proper attributes
    //
    //saveItem cycles through json file to mention of the 
    void CreateGameObjects()
    {
        foreach (string saveItem in saveItems)
        {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            //creates new prefab
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;

            //tells spawned item to deserialize itself
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
