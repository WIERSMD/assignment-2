﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : MonoBehaviour
{

    public float startSpeed;
    public float startSize;

    public float smokeColorValue;


    // Use this for initialization
    void Start()
    {
        SmokeStart();
    }

    //Assignes the size and speed of the smoke particles
    // sets the starting colour of the smoke to black for the first few seconds
    void SmokeStart()
    {
        ParticleSystem a = GetComponent<ParticleSystem>();

        a.startSize = startSize;
        a.startSpeed = startSpeed;
        a.startColor = new Color(smokeColorValue, smokeColorValue, smokeColorValue);


        Invoke("ReColour", 4);

        a.enableEmission = true;

    }

    //changes the colour to grey after being called
    void ReColour()
    {
        GetComponent<ParticleSystem>().startColor = Color.grey;
        smokeColorValue = GetComponent<ParticleSystem>().startColor.b;

    }
}
